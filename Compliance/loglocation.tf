Scenario: Log should be copied to particular destination
    Given I have AWS S3 bucket defined
    When it has log <folder>
    Then it must log location set as <value>

    Examples :
    |folder   |   value |
    |logs     |   /asd/logs  |
