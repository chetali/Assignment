Scenario: Ensure all S3 buckets are encrypted
    Given I have AWS S3 bucket defined
    Then encryption at rest must be enabled
