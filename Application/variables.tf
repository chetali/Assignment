variable "bucket_name" {
  description = "Unique Name of the S3 bucket"
  type        = string
  default     = "ps-proj-app-bucket"

}

variable "bucket_acl" {
  description = "ACL for the S3 bucket, default is private"
  type        = string
  default     = "private"

}

variable "tag_owner" {
  type        = string
  description = "Collection of project related tag"
  default     = "PS@gmail.com"
}


variable "tag_costcenter" {
  type        = string
  description = "Costcentre of the bucket"
  default     = "112233"
}

variable "vpc_name" {
  type        = string
  description = "Name of the VPC where the resources has to be created"
  default     = "PublicSapient"
}

variable "subnet_name" {
  type        = string
  description = "Name of the Subner where the resources has to be created"
  default     = "PS-Public"
}
variable "region" {
}
variable "instance_type" {}
variable "ami_id" {}
variable "key_pair" {}
variable "code_version" {}
