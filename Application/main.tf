data "aws_vpc" "ps_vpc" {
filter {
  name  = "tag:Name"
  values= ["${var.vpc_name}"]

}
state = "available"

}

data "aws_subnet" "ps_subnet" {
filter {
  name  = "tag:Name"
  values= ["${var.subnet_name}"]

}
state = "available"

}

resource "aws_s3_bucket" "app_bucket" {
  bucket = var.bucket_name

    tags =  {

      Owner       = var.tag_owner
      Costcenter  = var.tag_costcenter
    }
}

resource "aws_s3_bucket_versioning" "PS-versioning" {
  bucket = aws_s3_bucket.app_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_acl" "PS-ACL" {
  bucket = aws_s3_bucket.app_bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "PS-encyption" {
  bucket = aws_s3_bucket.app_bucket.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = "AES256"
    }
  }
}

resource "aws_security_group" "sg" {
  name        = "allow_ssh_http"
  description = "Allow ssh http inbound traffic"
  vpc_id      = data.aws_vpc.ps_vpc.id

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    description      = "Internet Access"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh_https"
    tag_owner       = var.tag_owner
    tag_costcenter  = var.tag_costcenter
  }
}


resource "aws_instance" "web" {
  ami             = var.ami_id
  instance_type   = var.instance_type
  subnet_id       = data.aws_subnet.ps_subnet.id
  security_groups = [aws_security_group.sg.id]
  key_name        = var.key_pair

  user_data = <<-EOF
#!/bin/bash
echo "*** Installing apache2"
sudo yum update -y
sudo yum install -y httpd.x86_64
sudo systemctl start httpd.service
sudo systemctl enable httpd.service
#sudo wget <s3_url_with_app>/"${var.code_version}" -P /var/www/html/
echo "*** Completed Installing apache"
EOF

  tags = {
    Name = "web_instance"
    tag_owner       = var.tag_owner
    tag_costcenter  = var.tag_costcenter
  }

}
